package io.c12.bala.dto;

/**
 * Question Type ENUM.
 */
public enum QuestionType {
  SELECT,
  MULTISELECT,
  TEXT,
  TEXTAREA,
  DATE
}
