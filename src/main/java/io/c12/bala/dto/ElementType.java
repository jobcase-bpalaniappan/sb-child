package io.c12.bala.dto;

/**
 * Element Type ENUM.
 */
public enum ElementType {
  INFORMATION,
  QUESTION,
  PAGE_BREAK
}
