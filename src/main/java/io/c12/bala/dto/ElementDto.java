package io.c12.bala.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ElementDto {
  private String domainId;
  private ElementType type;
  private String informationText;
  private Boolean isInputRequired;
  private ScreenerQuestionDto screenerQuestion;
}
