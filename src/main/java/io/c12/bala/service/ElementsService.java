package io.c12.bala.service;

import io.c12.bala.dto.StoredElement;

public interface ElementsService {
  StoredElement fetchElements(String jobPostingKey);
}
