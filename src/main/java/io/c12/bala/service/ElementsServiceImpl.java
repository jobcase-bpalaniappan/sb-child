package io.c12.bala.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.c12.bala.dto.AnswerOptionDto;
import io.c12.bala.dto.ElementDto;
import io.c12.bala.dto.ElementType;
import io.c12.bala.dto.NodeDto;
import io.c12.bala.dto.ScreenerQuestionDto;
import io.c12.bala.dto.StoredElement;
import io.c12.bala.entity.Elements;
import io.c12.bala.repository.ElementsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ElementsServiceImpl implements ElementsService {

  private final ElementsRepository elementsRepository;

  private final ObjectMapper objectMapper;

  @Override
  public StoredElement fetchElements(String jobPostingKey) {
    // List of element ids in right sequence from ATS Integration call.
    List<String> elementIds = getElementIdsFromAts(jobPostingKey);

    // Get list of elements based on the elementId list from ATS Integration call.
    var elementEntityList = this.elementsRepository.findAllByIdIn(elementIds);

    if (elementEntityList != null && !elementEntityList.isEmpty()) {
      return StoredElement.builder()
          .struct(generateStructList(elementIds, elementEntityList))
          .elements(generateElementMap(jobPostingKey, elementEntityList))
          .build();
    } else {
      return StoredElement.builder().build();
    }
  }

  // ----------------------------- Generate Structure -----------------------------

  /**
   * Generate Structure List with Pagination based on Page Breaks and maintain the order or element sequence from ATS calls.
   *
   * @param elementIds        from ATS integration call with sequence order of Elements.
   * @param elementEntityList List of Element entities from Database with Parent Child.
   * @return List of Pages with List of Nodes.
   */
  private List<List<NodeDto>> generateStructList(List<String> elementIds, List<Elements> elementEntityList) {
    List<List<NodeDto>> structureList = new LinkedList<>();
    int elementCounter = 1;

    // Generate HashMap with key as Element ID and value as collection of Element Entity list.
    var elementEntityIdMap = elementEntityList.stream().collect(Collectors.groupingBy(Elements::getId, Collectors.toList()));

    List<NodeDto> nodeDtoList = new LinkedList<>();
    for (var elementId : elementIds) {
      var elementEnitytList = elementEntityIdMap.get(elementId);
      if (!elementEnitytList.isEmpty()) {
        var elementEntity = elementEnitytList.get(0);

        // If Page Break is at first.. then ignore the page increment.
        if (elementCounter == 1 && elementEntity.getType().equals(ElementType.PAGE_BREAK)) {
          // Do nothing... Skip the page_break in the beginning.
        } else if (elementCounter > 1 && elementEntity.getType().equals(ElementType.PAGE_BREAK)) {
          structureList.add(nodeDtoList);
          nodeDtoList = new LinkedList<>();
        } else {
          nodeDtoList.add(createNodeStructure(elementEntity));
          elementCounter++;
        }
      }
    }
    structureList.add(nodeDtoList);
    return structureList;
  }

  /**
   * Create conditional question structure of element id and its children.
   *
   * @param elementEntity from Database with Parent Child.
   * @return {@code Node}
   */
  NodeDto createNodeStructure(Elements elementEntity) {
    NodeDto nodeDtoLevelZero = NodeDto.builder().id(elementEntity.getId()).build();    // Initialize Level Zero

    // -- Level One
    if (!elementEntity.getChildren().isEmpty()) {
      Map<String, NodeDto> childLevelOne = new HashMap<>();    // Initialize Level One Child
      elementEntity.getChildren().forEach(elementLevelOne -> {
        NodeDto nodeDtoLevelOne = NodeDto.builder().id(elementLevelOne.getId()).build();

        // -- Level Two
        if (!elementLevelOne.getChildren().isEmpty()) {
          Map<String, NodeDto> childLevelTwo = new HashMap<>();    // Initialize Level Two Child
          elementLevelOne.getChildren().forEach(elementLevelTwo -> {
            NodeDto nodeDtoLevelTwo = NodeDto.builder().id(elementLevelTwo.getId()).build();

            // Level Three
            if (!elementLevelTwo.getChildren().isEmpty()) {
              Map<String, NodeDto> childLevelThree = new HashMap<>();    // Initialize Level Three Child
              elementLevelTwo.getChildren().forEach(elementLevelThree -> childLevelThree.put(elementLevelThree.getCondValue(), NodeDto.builder().id(elementLevelThree.getId()).build()));
              // Set Child for Level Two if exists.
              nodeDtoLevelTwo.setChildren(childLevelThree);
            } // -- END Level Three

            childLevelTwo.put(elementLevelTwo.getCondValue(), nodeDtoLevelTwo);
          });
          // Set Child for Level One if exists.
          nodeDtoLevelOne.setChildren(childLevelTwo);
        } // -- END Level Two

        childLevelOne.put(elementLevelOne.getCondValue(), nodeDtoLevelOne);
      });

      nodeDtoLevelZero.setChildren(childLevelOne);   // Set Child for Level Zero if exists.
    } // -- END Level One
    return nodeDtoLevelZero;
  }

  // ----------------------------- Generate Elements -----------------------------

  /**
   * Generate element map based on Element Entity list from DB with Parent child.
   * Will be able to handle 3 levels.
   *
   * @param jobPostingKey     of the job
   * @param elementEntityList for entity.
   * @return Map of Key as {@code elementId} and value as {@code Element}.
   */
  Map<String, ElementDto> generateElementMap(String jobPostingKey, List<Elements> elementEntityList) {
    Map<String, ElementDto> elementDtoMap = new HashMap<>();
    // Level zero
    // And ignore page_breaks from the element list.
    elementEntityList.stream().filter(elements -> !elements.getType().equals(ElementType.PAGE_BREAK)).forEach(elementZero -> {
      elementDtoMap.put(elementZero.getId(), extractElement(jobPostingKey, elementZero));
      // Level one
      elementZero.getChildren().forEach(elementOne -> {
        elementDtoMap.put(elementOne.getId(), extractElement(jobPostingKey, elementOne));
        // Level Two
        elementOne.getChildren().forEach(elementTwo -> {
          elementDtoMap.put(elementTwo.getId(), extractElement(jobPostingKey, elementTwo));
          // Level Three
          elementTwo.getChildren().forEach(elementThree -> elementDtoMap.put(elementThree.getId(), extractElement(jobPostingKey, elementThree)));
        });
      });
    });
    return elementDtoMap;
  }

  /**
   * Extract Element from Element Entity.
   *
   * @param jobPostingKey of the job
   * @param elementEntity for entity.
   * @return Element Dto.
   */
  ElementDto extractElement(String jobPostingKey, Elements elementEntity) {
    var elementDto = ElementDto.builder().domainId(jobPostingKey).type(elementEntity.getType()).informationText(elementEntity.getInformationText()).isInputRequired(elementEntity.getIsInputRequired()).build();
    if (elementEntity.getType().equals(ElementType.QUESTION)) {
      elementDto.setScreenerQuestion(ScreenerQuestionDto.builder().id(elementEntity.getScreenerQuestion().getId()).type(elementEntity.getScreenerQuestion().getType()).question(elementEntity.getScreenerQuestion().getQuestion()).build());
      // Decode options if available.
      elementDto.getScreenerQuestion().setOptions(decodeAnswerOptions(elementEntity));
    }
    return elementDto;
  }

  /**
   * Decode Answer Options json String to DTO.
   *
   * @param elementEntity from Database to extract Answer Options.
   * @return List of {@code AnswerOption} or else returns {@code null}
   */
  List<AnswerOptionDto> decodeAnswerOptions(Elements elementEntity) {
    // Decode options if available.
    if (elementEntity.getScreenerQuestion().getOptions() != null && !elementEntity.getScreenerQuestion().getOptions().isEmpty()) {
      try {
        return objectMapper.readValue(elementEntity.getScreenerQuestion().getOptions(), new TypeReference<List<AnswerOptionDto>>() {
        });
      } catch (JsonProcessingException ex) {
        log.error("Error Decoding options JSON ", ex);
      }
    }
    return null;
  }

  /**
   * Create test data for ATS Integration system.
   *
   * @param jobPostingKey of the Job.
   * @return List of Element id for a Job Posting key.
   */
  List<String> getElementIdsFromAts(String jobPostingKey) {
    switch (jobPostingKey) {
      case "U-106983594501":
        // -- 3 Levels of questions with one page break in the middle
        return List.of("44eafa40-1e54-44d6-a13e-6667c5308a6a", "ad323845-4272-4c27-a404-10a914d6a86a", "fca2f2e0-1bba-455e-b4f8-32f2c92bc47d",
            "90091d6e-72c3-4e3a-9970-7de9d1b1854f", "691a2bc8-0972-4a99-8e27-97d91004f8a5");
      case "U-107002077653":
        // -- Page break at the beginning.
        return List.of("36699fc5-6cbd-424b-9a62-596e31062084", "4cc55b62-06b7-4d20-9f46-7920aec6d374", "f3a6b39a-bbff-478c-ac5d-b452115da1fb");
      case "U-107002077655":
        // -- Information and questions with page-break in the middle.
        return List.of("f9ab3e16-7f2c-40c7-b9f9-f0c8a5084e27", "2e133eb0-8907-45df-a615-8068400eca88", "a5cd91a5-ebb2-4876-8feb-2015b34aa19b",
            "c5844def-6612-480f-8c6d-186bd7c61f85", "3a9bc245-710e-4b9b-a400-f00dd6adb6b8", "5c6c1f2c-8682-4ad0-bed9-5375afe445dd");
      case "U-107002077689":
        // -- Two page-breaks in the middle.
        return List.of("3bb95c17-1a3b-4c61-98a0-51693a0da0c0", "127f455b-b8cd-45bf-b971-a656c1bbba89", "ecdc2ebe-df1b-472a-9af5-0c71fcbecb0d",
            "795e9ae9-460c-4785-aeae-7a4d1ebe6202", "19e4e569-c8ce-4c9b-9227-d513b6fab6dc", "8e048b81-26f5-4977-a653-32781da305f7", "460761d9-49da-4282-a3c2-e87985991fac");
      case "U-107002077690":
        return List.of("3");
      case "U-107002077687":
        return List.of("4");
      case "U-107002077691":
        return List.of("5");
      default:
        return List.of();
    }


  }
}
