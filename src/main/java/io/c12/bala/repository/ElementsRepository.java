package io.c12.bala.repository;

import io.c12.bala.entity.Elements;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ElementsRepository extends JpaRepository<Elements, String> {

  List<Elements> findAllByIdIn(List<String> elementIdList);
}
