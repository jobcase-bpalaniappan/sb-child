package io.c12.bala.controller;

import io.c12.bala.dto.StoredElement;
import io.c12.bala.service.ElementsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/elements")
@Slf4j
@RequiredArgsConstructor
public class ElementController {
  private final ElementsService elementsService;

  @GetMapping("/{jobPostingKey}")
  public ResponseEntity<StoredElement> elements(@PathVariable("jobPostingKey") String jobPostingKey) {
    return ResponseEntity.ok(elementsService.fetchElements(jobPostingKey));
  }
}
