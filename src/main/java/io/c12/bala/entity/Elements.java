package io.c12.bala.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.c12.bala.dto.ElementType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "elements")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Elements {
  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  @Column(name = "id", nullable = false)
  private String id;
  private String domainId;
  private Integer order;
  @Enumerated(EnumType.STRING)
  private ElementType type;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "screener_question_id")
  private ScreenerQuestion screenerQuestion;
  private String informationText;
  private Boolean isInputRequired;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(referencedColumnName = "id", name = "parent_id")
  @JsonBackReference
  private Elements parent;
  @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
  @JsonManagedReference
  private Set<Elements> children;
  private String condValue;
  @JsonIgnore
  @CreationTimestamp
  private LocalDateTime createdAt;
  @JsonIgnore
  @UpdateTimestamp
  private LocalDateTime updatedAt;
  @JsonIgnore
  private LocalDateTime deletedAt;

}