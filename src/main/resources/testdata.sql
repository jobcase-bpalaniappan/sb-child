
insert into question_service.screener_question(id, question, type, options)
values ('449da044-105b-4c9e-af22-c54873652a81', 'Are you a protected veteran?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"},{"label": "I decline to say","value":"I decline to say"}]'),
       ('7ce4ae77-e266-4bc2-b5c0-9bd06f75cde7', 'Preferred Joining date', 'DATE', null),
       ('dd0f01d3-7423-4059-9d0d-e0f74af25899', 'Gender', 'SELECT', '[{"label": "Male","value":"Male"}, {"label": "Female","value":"Female"}, {"label": "I decline to say","value":"I decline to say"}]'),
       ('de8e1aac-128d-4555-9790-e2dfeb6bb6be', 'Ethnic Origin', 'SELECT', '[{"label": "Hispanic/Latino","value":"Hispanic/Latino"},{"label": "Not Hispanic/Latino","value":"Not Hispanic/Latino"},{"label": "I decline to say","value":"I decline to say"}]'),
       ('d5ada528-f1c9-4d01-96e9-04a8a661a3a6', 'Are you willing to relocate?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('931a8373-1ac9-41c9-8d27-5a03b28e7151', 'Do you have 5 or more years of Microsoft technology experience?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('78db67dd-5fbe-4577-8c31-ea69cee759e6', 'What is your desired salary expectation?', 'TEXT', null),
       ('4602ea09-8e6b-44ac-b563-c9da4e459fd2', 'What license or certificate you have ?', 'TEXTAREA', null),
       ('ffeeb429-7e9a-4831-a4cd-4bfe1a9a8fbe', 'Any other information you what to share with us ?', 'TEXTAREA', null),
       ('2230746c-9d72-49d7-8f66-784f98f1b9d8', 'Enter today date to complete the application', 'DATE', null),
       ('0499e884-ad22-454b-9882-f85939c2d141', 'Permission to contact your previous / current employer ?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('63a4e66b-0d7f-420e-991d-ed11ee0b3767', 'Certification that all the information you have supplied is accurate', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('7ca9301f-b806-4e97-bfcb-c5fe4f216515', 'When would you be able to start?', 'DATE', null),
       ('492414a6-1f6e-4a3b-8d2f-8e5824741ee7', 'Do you have valid Driver license ?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('03745158-22a2-4b5c-957a-8c37aee8dc42', 'DO you have valid Commercial Drivers License (CDL) ?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('1df01d1c-933d-425d-894a-2e8314ff6b13', 'Do you have any major traffic violation, or DUI in last 3 years', 'SELECT', '[{"label": "Male","value":"Male"}, {"label": "Female","value":"Female"}, {"label": "I decline to say","value":"I decline to say"}]'),
       ('fde9b347-8af6-4269-82d9-585af1731c00', 'How years of driving commercial vehicles with CDL do you have', 'TEXT', null),
       ('25522843-050f-43b0-ba65-e122e07a6696', 'Willing to get Welders license paid by company?', 'SELECT', '[{"label": "Yes","value":"Yes"},{"label": "No","value":"No"}]'),
       ('62c49f76-c25b-4089-94fd-15e06ce9fb82', 'Choose your shift preference (Choose at least two)', 'MULTISELECT', '[{"label": "Early morning","value":"Early morning"}, {"label": "9am to 5pm","value":"9am to 5pm"}, {"label": "Afternoon","value":"Afternoon"}, {"label": "Night - extra pay","value":"Night - extra pay"}, {"label": "Rotating","value":"Rotating"}]'),
       ('178cbd48-70fa-4da9-be76-87b4162b945e', 'Choose the technologies you have used before', 'MULTISELECT', '[{"label": "Microsoft Word","value":"Microsoft Word"}, {"label": "Microsoft Excel","value":"Microsoft Excel"}, {"label": "QuickBooks","value":"QuickBooks"}, {"label": "Windows","value":"Windows"}, {"label": "MacOS","value":"MacOs"}]'),
       ('886584f3-86df-42ef-9580-828cd0f3e57e', 'Provide your CDL license number and expiration date', 'TEXT', null),
       ('b6e27c65-688f-4566-ae17-35e659b30829', 'Make and Model of vehicle you own, which will be used for delivery using person vehicle', 'TEXT', null),
       ('e231353d-66d9-4210-b553-4f8c96b2be34', 'Your location (City, State)', 'TEXT', null);

-- 3 Levels of questions with one page break in the middle
insert into question_service.elements(id, type, screener_question_id, information_text, is_input_required, parent_id, cond_value)
values ('44eafa40-1e54-44d6-a13e-6667c5308a6a', 'INFORMATION', null, 'Information One', false, null, null),
       ('ad323845-4272-4c27-a404-10a914d6a86a', 'QUESTION', 'de8e1aac-128d-4555-9790-e2dfeb6bb6be', null, true, null, null),
       ('0ba56742-7d5f-4751-b985-315a14e1718d', 'QUESTION', 'e231353d-66d9-4210-b553-4f8c96b2be34', null, true, 'ad323845-4272-4c27-a404-10a914d6a86a', 'Not Hispanic/Latino'),
       ('d562f76e-a8a2-41fe-9741-778789a51754', 'QUESTION', 'ee22bdea-ac58-4426-9898-8794c99092b9', null, true, 'ad323845-4272-4c27-a404-10a914d6a86a', 'I decline to say'),
       ('7b475cf3-0585-4922-9e09-f9e1ab3727e4', 'QUESTION', '78db67dd-5fbe-4577-8c31-ea69cee759e6', null, false, '0ba56742-7d5f-4751-b985-315a14e1718d', 'Other'),
       ('0ed40222-0d1b-4133-bdbe-19fd7d5a675b', 'QUESTION', '178cbd48-70fa-4da9-be76-87b4162b945e', null, false, 'd562f76e-a8a2-41fe-9741-778789a51754', 'Excel'),
       ('ac2658b2-d524-487a-be10-8040018cc282', 'QUESTION', '931a8373-1ac9-41c9-8d27-5a03b28e7151', null, false, 'd562f76e-a8a2-41fe-9741-778789a51754', '15'),
       ('ca0d880a-feb7-4122-8120-19465f342e47', 'QUESTION', '25522843-050f-43b0-ba65-e122e07a6696', null, false, '0ed40222-0d1b-4133-bdbe-19fd7d5a675b', 'I would like to give more information'),
       ('fca2f2e0-1bba-455e-b4f8-32f2c92bc47d', 'PAGE_BREAK', null, null, false, null, null),
       ('90091d6e-72c3-4e3a-9970-7de9d1b1854f', 'INFORMATION', null, 'Information Two', false, null, null),
       ('691a2bc8-0972-4a99-8e27-97d91004f8a5', 'QUESTION', 'e231353d-66d9-4210-b553-4f8c96b2be34', null, true, null, null);

-- Page break at the beginning.
insert into question_service.elements(id, type, screener_question_id, information_text, is_input_required, parent_id, cond_value)
values ('36699fc5-6cbd-424b-9a62-596e31062084', 'PAGE_BREAK', null, null, false, null, null),
       ('4cc55b62-06b7-4d20-9f46-7920aec6d374', 'INFORMATION', null, 'Information First', false, null, null),
       ('f3a6b39a-bbff-478c-ac5d-b452115da1fb', 'QUESTION', '449da044-105b-4c9e-af22-c54873652a81', null, true, null, null);

-- Information and questions with page break in the middle.
insert into question_service.elements(id, type, screener_question_id, information_text, is_input_required, parent_id, cond_value)
values ('f9ab3e16-7f2c-40c7-b9f9-f0c8a5084e27', 'INFORMATION', null, 'EEOC - Voluntary Self-Identification Survey', false, null, null),
       ('2e133eb0-8907-45df-a615-8068400eca88', 'INFORMATION', null, 'This employer is required to notify all applicants of their rights pursuant to federal labor laws. For further information, please review this notice from the Department of Labor: EEO is the Law poster. You may have additional rights pursuant to recent amendments to federal labor laws. Please review these protections from the EEO is the Law Supplement.\n\nThis employer is subject to certain nondiscrimination and/or affirmative action recordkeeping and reporting requirements which require the employer to invite applicants to voluntarily self-identify their race/ethnicity and gender.', false, null, null),
       ('a5cd91a5-ebb2-4876-8feb-2015b34aa19b', 'INFORMATION', null, 'This employer is a Government contractor required to take affirmative action to employ and advance in employment protected veterans pursuant to the Vietnam Era veterans'' Readjustment Assistance Act of 1974, as amended by the Jobs for Veterans Act of 2002. Government contractors are required to take affirmative action to employ and advance veterans in employment: 1. Disabled veterans; 2. Recently separated veterans; 3. Active duty wartime or campaign badge veterans; and 4. Armed Forces service medal veterans. We are also required to submit an annual report to the U.S. Department of Labor identifying the number of our employees belonging to each specified -protected veteran- category.', false, null, null),
       ('c5844def-6612-480f-8c6d-186bd7c61f85', 'PAGE_BREAK', null, null, false, null, null),
       ('3a9bc245-710e-4b9b-a400-f00dd6adb6b8', 'QUESTION', 'dd0f01d3-7423-4059-9d0d-e0f74af25899', null, true, null, null),
       ('88928c16-a831-4b53-8ed6-585987ff1e54', 'QUESTION', 'd5ada528-f1c9-4d01-96e9-04a8a661a3a6', null, true, '3a9bc245-710e-4b9b-a400-f00dd6adb6b8', 'Yes'),
       ('5c6c1f2c-8682-4ad0-bed9-5375afe445dd', 'QUESTION', 'e231353d-66d9-4210-b553-4f8c96b2be34', null, true, null, null);

-- Two page breaks in the middle.
insert into question_service.elements(id, type, screener_question_id, information_text, is_input_required, parent_id, cond_value)
values ('3bb95c17-1a3b-4c61-98a0-51693a0da0c0', 'QUESTION', 'd5ada528-f1c9-4d01-96e9-04a8a661a3a6', null, true, null, null),
       ('127f455b-b8cd-45bf-b971-a656c1bbba89', 'INFORMATION', null, 'We need few job related question questions which need to be answered.', false, null, null),
       ('ecdc2ebe-df1b-472a-9af5-0c71fcbecb0d', 'PAGE_BREAK', null, null, false, null, null),
       ('795e9ae9-460c-4785-aeae-7a4d1ebe6202', 'QUESTION', '0499e884-ad22-454b-9882-f85939c2d141', null, false, null, null),
       ('19e4e569-c8ce-4c9b-9227-d513b6fab6dc', 'QUESTION', '492414a6-1f6e-4a3b-8d2f-8e5824741ee7', null, true, null, null),
       ('23aaec2b-fefb-48c7-b463-442da0d7dd34', 'QUESTION', '1df01d1c-933d-425d-894a-2e8314ff6b13', null, true, '19e4e569-c8ce-4c9b-9227-d513b6fab6dc', 'Yes'),
       ('8e048b81-26f5-4977-a653-32781da305f7', 'PAGE_BREAK', null, null, false, null, null),
       ('460761d9-49da-4282-a3c2-e87985991fac', 'INFORMATION', null, 'By clicking Next or Finish button you are agreeing to share your contact information with the employer, who may contact you thru your Phone number or Email address.', false, null, null);