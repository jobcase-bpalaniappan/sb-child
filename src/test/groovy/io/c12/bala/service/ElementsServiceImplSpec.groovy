package io.c12.bala.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import io.c12.bala.entity.Elements
import io.c12.bala.entity.ScreenerQuestion
import io.c12.bala.repository.ElementsRepository
import spock.lang.Specification

class ElementDtoServiceImplSpec extends Specification {

  ElementsRepository elementsRepository = Mock()
  ObjectMapper objectMapper = new ObjectMapper()

  ElementsService elementsService = new ElementsServiceImpl(elementsRepository, objectMapper)

  def "Check if Page Break is at the beginning"() {
    given: "Create test Entity records"

    when: "Service method is called"

    then: "Verify data"
    1 == 1
  }

  def "Check with one Page Break in the middle"() {
    given: "Create test Entity records"

    when: "Service method is called"

    then: "Verify data"
    1 == 1
  }

  def "Check with two Page Break in the middle"() {
    given: "Create test Entity records"

    when: "Service method is called"

    then: "Verify data"
    1 == 1
  }

  def "Check with only Level zero"() {
    given: "Create test Entity records"

    when: "Service method is called"

    then: "Verify data"
    1 == 1
  }

  def "Check with only Level zero and Level one"() {
    given: "Create test Entity records"

    when: "Service method is called"

    then: "Verify data"
    1 == 1
  }

  def "decode answer options from Element Entity when options is null"() {
    given: "Element entity from Database"
    var element = Elements.builder().id("51295843-c7b7-40cd-a0f4-f6397466e34d")
        .screenerQuestion(ScreenerQuestion.builder().id("4f0b42e4-ea64-4917-9ea0-5c4cf63dbed2").options(null)
            .build()).build();

    when: "Service method is called"
    var response = elementsService.decodeAnswerOptions(element)

    then: "Verify Response"
    response == null
  }

  def "decode answer options from Element Entity"() {
    given: "Element entity from Database"
    var element = Elements.builder().id("51295843-c7b7-40cd-a0f4-f6397466e34d")
        .screenerQuestion(ScreenerQuestion.builder().id("4f0b42e4-ea64-4917-9ea0-5c4cf63dbed2")
            .options("[{\"label\": \"Yes\", \"value\":\"Yes\"},{\"label\": \"No\", \"value\":\"No\"}]")
            .build()).build();

    when: "Service method is called"
    var response = elementsService.decodeAnswerOptions(element)

    then: "Verify Response"
    response != null
    !response.isEmpty()
    response.size() == 2
    response.find {r -> r.label == "Yes"}.value == "Yes"
    response.find {r -> r.label == "No"}.value == "No"
  }

  def "decode answer options from Element Entity with invalid JSON"() {
    given: "Element entity from Database"
    var element = Elements.builder().id("51295843-c7b7-40cd-a0f4-f6397466e34d")
        .screenerQuestion(ScreenerQuestion.builder().id("4f0b42e4-ea64-4917-9ea0-5c4cf63dbed2")
            .options("{\"label\": \"Yes\", \"value\":\"Yes\"},{\"label\": \"No\", \"value\":\"No\"}")
            .build()).build();

    when: "Service method is called"
    var response = elementsService.decodeAnswerOptions(element)

    then: "Verify Response"
    response == null
  }

}